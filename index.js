let aj = require('ajlearnjs');

/**
 * @tests
 */

// aj.trace('{Math}.abs: Math.abs(-3252356.235)')({ Math_absFn: Math.abs(-3252356.235) });
// aj.trace('{Math}.abs: Math.abs')({ Math_absFn: Math.abs });
// console.log('Math.abs not JSON.stringified :', Math.abs);
// aj.trace('{Math}.E')(Math.E);
// aj.trace('{Math}.LN10')(Math.LN10);
// aj.trace('{Math}.LN2')(Math.LN2);
// aj.trace('{Math}.LOG10E')(Math.LOG10E);
// aj.trace('{Math}.PI')(Math.PI);
// aj.trace('{Math}.SQRT1_2')(Math.SQRT1_2);
// aj.trace('{Math}.SQRT2')(Math.SQRT2);
// aj.trace('{Math}.abs(-Math.PI)')(Math.abs(-Math.PI));
// aj.trace('{Math}.random()')(Math.random());
// aj.trace('{Math}.floor(0.49999)')(Math.floor(0.49999));
// aj.trace('{Math}.floor(0.99999)')(Math.floor(0.99999));
// aj.trace('{Math}.ceil(4.00001)')(Math.ceil(4.00001));
// aj.trace('{Math}.log(2)')(Math.log(2));
// aj.trace('{Math}.log10(2)')(Math.log10(2));
// aj.trace('{Math}.log10(Math.E)')(Math.log10(Math.E));
// aj.trace('global date = new Dateate()')(date = new Date());
// aj.trace('date.getFullYear().toString()')(date.getFullYear().toString());
// aj.trace('date.getMonth().toString()')(date.getMonth().toString());
// aj.trace('date.getDate()')(date.getDate());
// aj.trace('date.getDay().toString()')(date.getDay().toString());
// aj.trace('date.getTime() -> ms since midnight 1Jan1970')(date.getTime());
// aj.trace('date.getTime().toString()')(date.getTime().toString());
// aj.trace('Date.now() == new Date.getTime()')(Date.now());
// aj.trace('getting the year with gp eval optr, (...globalDefns.lastArgRetned)')((sec = 1000, min = 60 * sec, hr = 60 * min, d = 24 * hr, y = 365 * d, Math.floor(date / y) + 1970));
// aj.trace('getting the year with =>Fn, (...fnDefns) => ({retnedObj}), executed immediately')(((date, arg_sec = 1000, arg_min = 60 * arg_sec, arg_hr = 60 * arg_min, arg_d = 24 * arg_hr, arg_y = 365 * arg_d) => Math.floor(date / arg_y) + 1970)(date = new Date()));
// aj.trace('getting the year with =>Fn, (...fnDefns) => ({retnedObj}), executed immediately')(((date = new Date(), arg_sec = 1000, arg_min = 60 * arg_sec, arg_hr = 60 * arg_min, arg_d = 24 * arg_hr, arg_y = 365 * arg_d) => Math.floor(date / arg_y) + 1970)());
// aj.trace('\'sec\' initialized in ms')(sec);
// aj.trace('global \'sec\' exist?')(global.hasOwnProperty('sec'));
// aj.trace('global \'arg_sec\' exist?')(global.hasOwnProperty('arg_sec'));
// aj.trace('15 mod 6')(15 % 6);
// aj.trace('new Date(2018,10,18)')(new Date(2018, 10, 18));
// aj.trace('String.toString method')(String.toString);

// ***************************************************************************************************

// console.log({ mylistx: aj._range(10) });
// console.log({ mylisty: aj._range(3, 10, 2) });
// console.log({ mylistz: aj._range(10, 3) });
// console.log({ list1: aj._range(10) });
// // [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
// console.log({ list2: aj._range(65, 69) });
// // [65, 66, 67, 68]
// console.log({ list3: aj._range(10, -10.1, -5) });
// // [10, 5, 0, -5, -10]
// console.log({ list4: aj._range(10, 1) });
// // []
// for (let i of aj._range(10, -10.1, -5)) console.log({ i });
// for (let i of 'aj was here.') console.log({ i });
// // for (let i in 'aj was here.') console.log(i + ':', 'aj was here.'[i].repeat(3));
// for (let i in 'aj was here.'.repeat(2)) console.log(i + ':', 'aj was here.'.repeat(2)[i].repeat(6));


// _______

// let myChrs = aj._range(127744, 128501), i = Math.min(...myChrs), j = Math.max(...myChrs); console.log({ i, j });
// // console.log({ inBrackets: aj.f_strOrArrReverse(myChrs) });

// // for (let i of aj._range(127744, 128292)) i = String(i), console.log({ i, chr: String.fromCodePoint(i) });
// myChrs = [];
// for (let i of aj._range(127744, 128501)) i = String(i), myChrs.push({ i, chr: String.fromCodePoint(i) });
// // console.log({ myChrs: myChrs[4].i, myChrs: myChrs[4].chr, myChrs });
// // for (let i in aj._range(128009, 128100)) console.log(i + ':', String.fromCodePoint(i));

// aj.f_chrBarMeter('sig', 185, 150, '%&');
// aj.f_chrBarMeter('sig', 185, 150, '   %&');
// aj.f_chrBarMeter('sig', 185, 150, ' ');

// const codePtChrs = [1012, 2604, 3051, 3485, 3940, 4240, 5555, 9020, 9030, 9040, 9050, 9200, 9210, 9400, 9410, 9420, 9430, 9440, 9460, 9470, 9600, 9610, 9611, 9612, 9613, 9614, 9615, 9617, 9618, 9619, 9620, 9640, 9650, 9660, 9670, 9690, 60000];
// var meter = [];
// for (let deg = 0, level = 0, maxLvl = 120, k = 0, chr;
//     deg <= 360 * 2;
//     deg += 10, k >= codePtChrs.length ? k = 0 : k++ , [level, chr] = (deg > 90 * 2 && deg < 180 * 2) || (deg > 270 * 2 && deg < 360 * 2) ? [Math.random() * maxLvl, String.fromCodePoint(codePtChrs[k])] : [Math.sin(Math.PI / 180 * deg) * maxLvl / 2 + maxLvl * 0.2, String.fromCodePoint(myChrs[deg].i)]) meter.push(aj.f_chrBarMeter('vol', level, maxLvl, chr, false));
// for (let deg = 0, level = 0, maxLvl = 120, k = 0, chr;
//     deg <= 360 * 2;
//     deg += 10, k >= codePtChrs.length ? k = 0 : k++ , [level, chr] = (deg > 90 * 2 && deg < 180 * 2) || (deg > 270 * 2 && deg < 360 * 2) ? [Math.random() * maxLvl, String.fromCodePoint(codePtChrs[k])] : [Math.sin(Math.PI / 180 * deg) * maxLvl / 2 + maxLvl * 0.2, String.fromCodePoint(myChrs[deg].i)]) meter.push(aj.f_chrBarMeter('vol', level, maxLvl, String.fromCodePoint(9613)));

// console.log('\n\n\n', meter.length); meter.push('\n');

// // const sleep = ms => new Promise(res => setTimeout(res, ms));
// // var print = async (msg, ms) => { process.stdout.write(msg); await sleep(ms); }

// // let danceMeter = async (strArr, ms) => {
// //     for (let i = 0; i < meter.length; i++)  await print(meter[i], ms);
// // }

// aj.danceMeter(meter, 100);


// ***************************************************************************************************

// aj.trace('aj.f_capitalize(\'hi thERE, my namE iS andrew tAN     chOon yew.\')')(aj.f_capitalize('hi thERE, my namE iS andrew tAN     chOon yew.'));
// aj.trace('aj.f_capitalize(\'hi thERE, my       namE iS andrew tAN     chOon yew.\', \'words\')')(aj.f_capitalize('hi thERE, my       namE iS andrew tAN     chOon yew.', 'words'));
// aj.trace('aj.f_capitalize(\'hi thERE, my namE iS andrew tAN chOon yew.   how do you              do?\', \'sentences\')')(aj.f_capitalize('hi thERE, my namE iS andrew tAN chOon yew.   how do you              do?', 'sentences'));
// aj.trace('aj.f_capitalize(\'hi thERE, my namE iS andrew tAN chOon yew.     how do you do?\', \'allCaps\')')(aj.f_capitalize('hi thERE, my namE iS andrew tAN chOon yew.     how do you do?', 'allCaps'));
// aj.trace('aj.f_capitalize(\'hi thERE, my namE iS andrew tAN chOon yew.    how do you do?\', \'words\')')(aj.f_capitalize('hi thERE, my namE iS andrew tAN chOon yew.    how do you do?', 'words'));

// ***************************************************************************************************

// aj.trace('tf_getYear()')(aj.tf_getYear(1547206326864));
// aj.trace('tf_getYear()')(aj.tf_getYear(1547622369150));
// console.table(aj.tf_getYear(1547622369150));
// console.dir(aj.tf_getYear(1547622369150)); console.log(aj.tf_getYear(1547622369150));
// console.table(['aj', 123, true]);
// console.dir(['aj', 123, true]);
// console.log(['aj', 123, true]);

// ***************************************************************************************************

// aj.trace('aj.f_strnum2OrdVals_2387959823')(aj.f_strnum2OrdVals('2387959823'));
// aj.trace('aj.f_strnum2OrdVals_2387959823.join(\'\'')(aj.f_strnum2OrdVals('2387959823').join(''));    // joining numbers make a string again - not our purpose.
// aj.trace('aj.f_strnum2OrdVals_2018-4-23')(aj.f_strnum2OrdVals('2018-4-23', '-'));
// aj.trace('aj.f_strnum2OrdVals_2018-4-23')(aj.f_strnum2OrdVals('2018@gmail.com', '@'));
// aj.trace('aj.f_strnum2OrdVals(\'hi thERE, my namE iS andrew tAN     chOon yew.\')')(aj.f_strnum2OrdVals('hi thERE, my namE iS andrew tAN     chOon yew.', ' '));
// aj.trace('aj.f_strnum2OrdVals(\'hi thERE, my namE iS andrew tAN chOon yew. how do you do?   77   436347.2\', \' \')')(aj.f_strnum2OrdVals('hi thERE, my namE iS andrew tAN chOon yew. how do you do?   77   436347.2', ' '));
// aj.trace('aj.f_strnum2OrdVals(\'ℱїαт ϟεℯкḯηℊ Ṳᾔⅾεґṧ⊥αη∂їηℊ\')')(aj.f_strnum2OrdVals('ℱїαт ϟεℯкḯηℊ Ṳᾔⅾεґṧ⊥αη∂їηℊ').map(ch => ch + ' , ' + ch.codePointAt(ch[0])));

// ***************************************************************************************************

// aj.trace('aj.f_strnum2OrdVals_newDate_2018_10_18')(aj.f_strnum2OrdVals(aj.f_todayOrDate2YMD(date = new Date(2018, 10, 18), '.'), divdr = '.').join('|'));
// aj.trace('newDate_2018_10_18 - notice timezone makes date diff from .getDate() method')(date); console.log({ date: date.getDate() });
// aj.trace('aj.f_strnum2OrdVals_newDate_')(aj.f_strnum2OrdVals(aj.f_todayOrDate2YMD(), divdr = '-'));

// ***************************************************************************************************


// const cuppageDesign = [
//     ['andrew', 7],
//     ['benny', 6],
//     ['chelsea', 5],
//     ['dandelion', 4],
//     // ['elijah', 7],
//     // ['fleur', 6],
//     // ['gratitude', 8],
//     // ['hogwarts', 5],
//     // ['ithink', 6],
//     ['jennifer', 10],
//     // ['krabi', 6],
//     // ['lincoln', 7],
//     // ['maria', 6],
//     // ['nanotubial', 4],
//     // ['oscar', 6],
//     // ['penelope', 6],
//     // ['queen', 7],
//     // ['rustic', 6],
//     // ['starship', 5],
//     // ['temporal', 6],
//     // ['umpire', 5],
//     ['valiant', 6],
//     ['well', 6]
// ];
// var cuppage = cuppageDesign.map((arr, idx) => {
//     let [x, y] = arr;
//     let z = `${idx + 1}_` + x.charAt(0).toLowerCase() + x.charAt(1).toLowerCase() + x.charAt(2).toLowerCase() + '_' + (y >= 8 ? 'Suite' : 'Rm') + '_' + y + 'pax';
//     [y, x, z] = [x, y, z];
//     return [x, y, z];
// }).sort((a, b) => a[0] == b[0] ? 0 : a[0] < b[0] ? -1 : 1);
// aj.trace('cuppage')(cuppage);
// console.log({ cuppage: JSON.stringify(cuppage) });

// for (let room of cuppage) room.push(aj.f_randActyBlot(3, 2));
// aj.trace('cuppage')(cuppage);

// console.log({ cuppage: JSON.stringify(cuppage) });

// console.log(aj.f_chkOrBookRm(cuppage));
// console.log(aj.f_chkOrBookRm(cuppage, 6));
// console.log(aj.f_chkOrBookRm(cuppage, 6, '2019.1.9', 1200, 3));
// console.log(aj.f_chkOrBookRm(cuppage, 9, '2019.1.9', 0930, 6, 'Augustus Tan.aug2019@gmail.com.94556047')); /* console.log({ cuppage: JSON.stringify(cuppage) }); */
// console.log(aj.f_chkOrBookRm(cuppage, 6, '2019.1.9', 1200, 3, 'jo')); console.log({ cuppage: JSON.stringify(cuppage) });
// console.log(aj.f_chkOrBookRm(cuppage, 6, '2019.1.9', 1200, 3)); /* console.log({ cuppage: JSON.stringify(cuppage) }); */


// ***************************************************************************************************

// aj.trace('aj.f_arrSort([\'20\', 40, \'bz\', 100, \'10\', 1, 5, 25, 10, \'Zinger\', \'oppa\', \'apple\', \'Banana\', \'balm\'])')(aj.f_arrSort(['20', 40, 'bz', 100, '10', 1, 5, 25, 10, 'Zinger', 'oppa', 'apple', 'Banana', 'balm']));
// aj.trace('aj.f_arrSort([\'20\', 40, \'bz\', 100, \'10\', 1, 5, 25, 10, \'Zinger\', \'oppa\', \'apple\', \'Banana\', \'balm\'], 1)')(aj.f_arrSort(['20', 40, 'bz', 100, '10', 1, 5, 25, 10, 'Zinger', 'oppa', 'apple', 'Banana', 'balm'], 1));
// let points = [40, 100, 1, 5, 25, 10, 'Zinger', 'oppa', 'apple', 'Banana', 'balm'];
// console.log(points.map(ele => typeof ele == 'number' ? ele : ele.toLowerCase()).sort((a, b) => a == b ? 0 : a < b ? -1 : 1));
// points = 'ƒºαßΓπΣσµτΦΘΩδ∞φε∩≡±≥≤⌠⌡÷≈°∙·√ⁿ²';
// aj.trace('aj.f_arrSort(aj.f_strnum2OrdVals(\'ƒºαßΓπΣσµτΦΘΩδ∞φε∩≡±≥≤⌠⌡÷≈°∙·√ⁿ²\'))')(aj.f_arrSort(aj.f_strnum2OrdVals(points), -1).map(ch => `${ch} , ${ch.charCodeAt(ch[0])}`));

// ***************************************************************************************************

// aj.trace('reverse this string: ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890-=`~!@#$%^&*()_+[]\\{}|;:\",.<>?\/\'\n')(aj.f_strOrArrReverse('ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890-=`~!@#$%^&*()_+[]\\{}|;:\",.<>?\/\''));
// aj.trace('today\'s date')(aj.f_todayOrDate2YMD());
// aj.trace('reverse today\'s date, or aj.f_strOrArrReverse(aj.f_todayOrDate2YMD(), \'-\', \'.\')')(aj.f_strOrArrReverse(aj.f_todayOrDate2YMD(), '-', '.'));
// aj.trace('aj.f_strOrArrReverse([\'aj\', 123, true], \'zabo\', \'|\')')(aj.f_strOrArrReverse(['aj', 123, true], 'zabo', '|'));
// aj.trace('aj.f_strOrArrReverse(235236.326326)')(aj.f_strOrArrReverse(235236.326326));
// aj.trace('aj.f_strOrArrReverse(\'235236.326326\')')(aj.f_strOrArrReverse('235236.326326'));

// ***************************************************************************************************

// aj.trace('aj.f_rotajF(\'Andrew Tan Choon Yew\')')(aj.f_rotajF('Andrew Tan Choon Yew'));
// aj.trace('aj.f_rotjaF(\'Andrew Tan Choon Yew\')')(aj.f_rotjaF('Andrew Tan Choon Yew'));
// aj.trace('aj.f_rotjaF(aj.f_rotajF(\'Andrew Tan Choon Yew\'))')(aj.f_rotjaF(aj.f_rotajF('Andrew Tan Choon Yew')));
// aj.trace('aj.f_rotajF(aj.f_rotjaF(\'Andrew Tan Choon Yew\'))')(aj.f_rotajF(aj.f_rotjaF('Andrew Tan Choon Yew')));
// aj.trace('aj.f_rotajG(\'Andrew Tan Choon Yew\')')(aj.f_rotajG('Andrew Tan Choon Yew'));
// aj.trace('aj.f_rotajG(\'jskJIrKKswx@LjHsP.nrj\')')(aj.f_rotajG('jskJIrKKswx@LjHsP.nrj'));
// aj.trace('aj.f_rotjaG(\'Andrew Tan Choon Yew\')')(aj.f_rotjaG('Andrew Tan Choon Yew'));
// aj.trace('aj.f_rotjaG(aj.f_rotajG(\'Andrew Tan Choon Yew\'))')(aj.f_rotjaG(aj.f_rotajG('Andrew Tan Choon Yew')));
// aj.trace('aj.f_rotajG(aj.f_rotjaG(\'Andrew Tan Choon Yew\'))')(aj.f_rotajG(aj.f_rotjaG('Andrew Tan Choon Yew')));
// aj.trace('HNqMrkxMrjx@uHM')(aj.f_rotjaF('HNqMrkxMrjx@uHM'));

// ***************************************************************************************************

// aj.trace('aj.f_funLtrs(\'Faith seeking understanding.\')')(aj.f_funLtrs('Faith is seeking wisdom in understanding, and surely finding Her Unified-and-Absolute Truth in Him.  The LORD Himself guarantees this: that if you \'fiat voluntas tua\' to His will-for-you, your name can be found in His Book of Life by the Sanctifying Grace of The Holy Ghost, opened to all and offered for anyone.  To this, you must be humble and work out your salvation (for yourself) with fear and trembling, as St-Paul urges us to put on the Mind of Christ.  Let Divine-Fear extract out your wicked heart, so that the Passion-of-Christ may enter your mind to give you a renewed heart of goodness, one that is pleasing in the courts of the Almighty Lord.  For the LORD maketh all things new.  AJ123'));
// aj.trace('aj.f_funLtrs(\'Faith seeking understanding.\')')(aj.f_strnum2OrdVals('Faith is seeking wisdom in understanding, and surely finding Her Unified-and-Absolute Truth in Him.  The LORD Himself guarantees this: that if you \'fiat voluntas tua\' to His will-for-you, your name can be found in His Book of Life by the Sanctifying Grace of The Holy Ghost, opened to all and offered for anyone.  To this, you must be humble and work out your salvation (for yourself) with fear and trembling, as St-Paul urges us to put on the Mind of Christ.  Let Divine-Fear extract out your wicked heart, so that the Passion-of-Christ may enter your mind to give you a renewed heart of goodness, one that is pleasing in the courts of the Almighty Lord.  For the LORD maketh all things new.  AJ123 with STRINGS, ARRAYS & OBJECTS!', ' ').map(word=> aj.f_funLtrs(word)).join('  _|_  '));

// ***************************************************************************************************

// aj.trace('aj.f_sanitizeStr(\'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890~!@#$%^&*()_+-=`][{}|:",./?><\'\\        \\t\\n\\f\\r\\v\\0\')')(aj.f_sanitizeStr('ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890~!@#$%^&*()_+-=`][{}|:",./?><\'\\        \t\n\f\r\v\0'));

// ***************************************************************************************************

// console.log(aj.f_strEncDec('Tan Choon Yew')); console.log(aj.f_strEncDec('testString0123456789', 1, 13));
// console.log(aj.f_strEncDec('apptreme.sg', 0, 42243)); console.log(aj.f_strEncDec('ꕤꕳꕳꕷꕵꕨꕰꕨꔱꕶꕪ', 1, 42243));
// console.log(aj.f_strEncDec('apptreme.sg', 0, 72243)); console.log(aj.f_strEncDec('apptreme.sg', 1, 42243));
// let idStr = aj.f_strEncDec(JSON.stringify({ legalFullNameID: 'anonymous\' Full Legal Name', emailID: 'alias@someEmail.domain', IDNum: 'SABCDEFGZ', race: 'Chinese', DOB: 'dd-mmm-yyyy', gender: 'Y', bloodGp: 'X\+', COB: 'State, Country', nationality: 'Singapore', postCode: '6-digit', postSt: 'Example St.', postBlk: 'XXXB', postUnit: '#YY-ZZ', dateOfAddr: '25-aug-2000', dateOfIssue: '28-feb-1994' }), 0);
// console.log({ idStr })
// console.log(/* JSON.parse */(aj.f_strEncDec(idStr, 1, 40967)), aj.f_strEncDec(idStr, 1, 42243));


// aj.trace('aj.f_encDecName(\'   this is just a sentence, not a name.\')')(aj.f_encDecName('   this is just a sentence, not a name.'));
// aj.trace('aj.f_encDecName(\'   andrew tan choo yew\')')(aj.f_encDecName('   andrew tan choo yew'));
// aj.trace('aj.f_encDecName(\'   ajmindsoffire@gmail.com\')')(aj.f_encDecName('   ajmindsoffire@gmail.com'));
// aj.trace('aj.f_encDecName("This ꕌꕶꔣꕍꕸꕶꕷꔣꕄꔣꕖꕨꕱꕷꕨꕱꕦꕨꔯꔣꕑꕲꕷꔣꕄꔣꕑꕤꕰꕨꔱ", -1)')(aj.f_encDecName("This ꕌꕶꔣꕍꕸꕶꕷꔣꕄꔣꕖꕨꕱꕷꕨꕱꕦꕨꔯꔣꕑꕲꕷꔣꕄꔣꕑꕤꕰꕨꔱ", -1));
// aj.trace('aj.f_encDecName("Andrew ꕗꕤꕱꔣꕆꕫꕲꕲꔣꕜꕨꕺ", -1)')(aj.f_encDecName("Andrew ꕗꕤꕱꔣꕆꕫꕲꕲꔣꕜꕨꕺ", -1));
// aj.trace('aj.f_encDecName("ajmindsoffire @ ꕪꕰꕤꕬꕯꔱꕦꕲꕰ", -1)')(aj.f_encDecName("ajmindsoffire @ ꕪꕰꕤꕬꕯꔱꕦꕲꕰ", -1));
// aj.trace('aj.f_encDecName(\'   andrew tan choo yew\')')(aj.f_encDecName('   andrew tan choo yew', 0, 6000));
// aj.trace('aj.f_encDecName("Andrew ោ៑៞ថឳ៘៟៟ថ៉៕៧", -1, 6000)')(aj.f_encDecName("Andrew ោ៑៞ថឳ៘៟៟ថ៉៕៧", -1, 6000));
// aj.trace('aj.f_encDecName(\'   ajmindsoffire@gmail.com\')')(aj.f_encDecName('   ajmindsoffire@gmail.com', 0, 15000));
// aj.trace('aj.f_encDecName("ajmindsoffire @ 㫿㬅㫹㬁㬄㫆㫻㬇㬅", -1, 15000)')(aj.f_encDecName("ajmindsoffire @ 㫿㬅㫹㬁㬄㫆㫻㬇㬅", -1, 15000));
// aj.trace('aj.f_encDecName("ajmindsoffire @ 㫿㬅㫹㬁㬄㫆㫻㬇㬅", -1, 17000)')(aj.f_encDecName("ajmindsoffire @ 㫿㬅㫹㬁㬄㫆㫻㬇㬅", -1, 17000));
// aj.trace('aj.f_encDecName("ajmindsoffire @ 㫿㬅㫹㬁㬄㫆㫻㬇㬅", -1, 17)')(aj.f_encDecName("ajmindsoffire @ 㫿㬅㫹㬁㬄㫆㫻㬇㬅", -1, 17));

// ***************************************************************************************************


// o1 = aj.tfo_objFty('Andrew', 'S0239667J', '1967-4-23', 324543, 'all other args can be seen unless you return nothing back..');
// aj.trace('visitCntr')(o1.accessCnt());
// aj.trace('o1 b4 trying to change the obj')(o1); aj.trace('visitCntr')(o1.accessCnt());
// aj.trace('o1 profile b4 trying to change the obj')(o1.profile()); aj.trace('visitCntr')(o1.accessCnt());
// aj.trace('o1.name = \'James\'')(o1.name = 'James'); aj.trace('visitCntr')(o1.accessCnt());
// aj.trace('o1.IC = 23523643')(o1.IC = 23523643); aj.trace('visitCntr')(o1.accessCnt());
// aj.trace('o1.bDate = \'TRY\'')(o1.bDate = 'TRY'); aj.trace('visitCntr')(o1.accessCnt());
// aj.trace('o1.addAPropty = \'senile\'')(o1.addAPropty = 'senile'); aj.trace('visitCntr')(o1.accessCnt());
// aj.trace('o1 after trying to change the obj')(o1); aj.trace('visitCntr')(o1.accessCnt()); aj.trace('o1.hasOwnProperty(\'addAPropty\')')(o1.hasOwnProperty('addAPropty'));
// aj.trace('o1 profile after trying to change the obj')(o1.profile()); aj.trace('visitCntr')(o1.accessCnt());
// aj.trace('o1\'s .accessCnt method print')(o1.accessCnt = 100);
// aj.trace('o1\'s .accessCnt method print')(o1.accessCnt); aj.trace('visitCntr')(o1.accessCnt()); console.log({ 'o1.accessCnt method': o1.accessCnt });
// aj.trace('visitCntr')(o1);
// aj.trace('visitCntr')(o1);
// o1.accessCnt();
// o1.accessCnt();
// aj.trace('visitCntr2x')([o1.accessCnt(), o1.accessCnt()]);
// aj.trace('arg')([o1.next(), o1.next()]);
// aj.trace('arg')(o1.next());
// aj.trace('arg')(o1.next());
// aj.trace('arg')(o1.next());
// aj.trace('arg')(o1.next());
// aj.trace('[arg,o1]')([o1.next(), o1]);


// let unicodes = ['unicodes'];
// for (let i of aj._range(parseInt('3b1', 16), parseInt('3c9', 16) + 1)) unicodes.push({ codePtVal: i, escSeq: 'x+' + (i).toString(16), chr: String.fromCodePoint(i) });
// aj.trace('makecodes')(unicodes);

// console.log({ greekAlphabetCodes: aj.GreekAlphabet });
// console.log(String.fromCharCode(parseInt(aj.GreekAlphabet[1].unicodeHex, 16)));
// aj.trace('Greek Alphabet Codes')([aj.GreekAlphabet, aj.GreekAlphabet[1].chr]);


// for (let [idx, ent] of aj.Codes.entries())
//     console.log({ idx, Codes: ent });
// for (let code of aj.Codes) if (/currency/gi.test((code[0]))) console.log({ code });
// for (let [i, v] of aj.Codes.entries()) {
//     for (let [idx, val] of v.entries())
//         if (/gamma/gi.test(val.desc)) console.log({ val })
// }
// aj.trace('Codes')([aj.Codes[0], aj.Codes[1]])

// console.log({ ajFnsVer: aj.version });

// let iterable = {
//     0: 'iter',
//     1: 123,
//     2: 'aj',
//     length: 3,
//     [Symbol.iterator]() {
//         let index = 0;
//         return {
//             next: () => ({ done: index >= this.length, value: this[index++] })
//         }
//     }
// }



aj.serve.appStartNUseAllMware(6707); // *** confirmed can 'load balance' required daemons across all ports listened to here. *** // 
const wfi = aj.serve.appStartNUseAllMware(6701);
const maybe = aj.serve.appStartNUseAllMware(6702);
const fartry = aj.serve.appStartNUseAllMware(7700);

const path = aj.serve.path;
const fs = aj.serve.fs;
const multer = aj.serve.multer;
const sha256 = aj.serve.sha256;
const jwt = aj.serve.jwt;

console.log({ sha256: sha256(aj.serve.SERVER_SALT), jwt: jwt.encode('Andrew Tan', '123'), decode: jwt.decode('eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.IkFuZHJldyBUYW4i.K3cyWNEw8Us-WaIugh79rtx7p2NCdu1V6Uj5tRGqLUs', '123') });
console.log({ token: aj.f_genIssueToken(aj.f_rotajF('Andrew Tan Choon Yew'), aj.f_strEncDec('NaiShan667'), 'rw_', 260000) });

const admin = require('firebase-admin');
// var serviceAccount = require('./property-apptreme-firebase-adminsdk-dxwdd-689613fb57.json');
admin.initializeApp({
    credential: admin.credential.cert({
        "type": "service_account",
        "project_id": "cloud-firestore-test-2320f",
        "private_key_id": "42bb5452965734bb2581e03a66fe295cd0526eea",
        "private_key": "-----BEGIN PRIVATE KEY-----\nMIIEvQIBADANBgkqhkiG9w0BAQEFAASCBKcwggSjAgEAAoIBAQDHJLzj1Nb7Csow\nd1RjAO5vfE8cGwASgNpcuEj14z3bND6O7iwHD5NBuukoZ/741xwq90x/s5jcL7dh\nQ/QpJMBhAXTHJwsRGCS/3MOyMBfb8mQz88zence4RZaAlfGv4Ag4RcMMgjlXAWgm\nXXUJ3JZUHfIJKmGIC/MqOUYmBiU26Fv0j6Q5ua5SNkDC/KbJlHE83OdRJhmqDcVm\nm6AFNQHBYvUC/CrCtv9vSKxNByjELseo0/PBIlnAzhsfzx8TF04ASLg0Mo7ikAuh\nUyUeATlEDtjQeam4zvCK068yvywnwE464eJiyQGiwaFReL9h3JdomdzEMz3QHPiR\nPeJ9s0oBAgMBAAECggEACqbFuPnoCxgR+x2m1H927AeeGPFow5SYxAonUjThWMQm\nmcaAxwU86cXG4B5lM4dt6O5pQmX8xb+CSMrgWFha4dVjcEjU/V5pOMnkvbciJOWG\nGTFFF/MgfOlb72zAYe59rLougJDGjbIe5qPBCtH9nb+Bnp6MW5Z9XxjJI7zYaLiq\nBal2+SR5L+sE9MGK12pYN4HMElmJ6cGzakMcmb6utKHWsV+H/Oox7+r3ysCpk4nQ\noNBUNJk/DGUGWBkOKRyU99M0VijDDrCrBvzQ36C7qs6JCGd36ispRehJhuV8Ro5i\nNFpxYg8kRRO6UoTCr4CrZNQQ2pd3ZYgnHPqrhGK2MQKBgQDyslTM/TbCJsX5pHka\nSfBk6RCKiJF3xbl2CG/0GSTDfo3IHFOMY/2Usx6JSRcQaYxGlv99tpxRlTlIqN9l\nEPJeBSPFwUFo53nfI3hjG0HQtLrlCb08y4AERpo182ofXe2rWIf/jYy4kSXkgPZe\nFgf8Cp6EsPwHkMYX1sBUSA63YwKBgQDSDz4a6qi6k7Zr8YnoWd6x7NKgDyhU615Z\n/AbAgM7fE/SUXwwJc05KWXUKKAftl3EGegySgR5BZ48rSU+r0/uYghgU6uz4QR6e\nDnfuk4Zl3fGdYmcZ8OGccB/OzXmgTyFbgLvQ+gFM8qSkgFbD6xYfsDTFIt6mWdu/\nicUXF0swSwKBgAkFX+mOr+5fipRQ7L9xaCIYF6lOkjmle9NLVP35wUgS+CosA18A\niHhyf/wSX20Yv0IQcsa1dvIBXfO5czRKgPfFsSK0oD3J/Nmyc9MbPEMJtLi2t/X+\nim+eQDUpKxSnZSItVEDHfieOHZ50vLOalglkS6ga/AM7+mEAdn2rgACJAoGBALw1\nrA+x8Tw63VgaMtZcr4v8BTwkWJ69xOwbRHF29+QonRvTjZVqGtvDC3ruIyxeZIbJ\nFDqwNcukXkMKjLgIV4VuCuWzGF/W+PflPtAknmbnVXUOlhJIId1pGRbSRAe+3sR9\ns8qqNkAbqCk8VIF2QXxjBOUdOSETojEb39uwo03BAoGAKN6ByhZwTIHHIMbGWUgL\nn0z7aAnn7sZgd2dXuY07d0BNOO4/XGYkLuXgDtT8nWSpM0liceU1GHLPQ3GAOPk7\nVnJha+tQtb84uSt/MNsC+M/eHMo7M1xLZevRKBWwQb03pEe1jHfYbmtbIl7Av75l\nQHbYq3kfC6FfGQdtI3EviI0=\n-----END PRIVATE KEY-----\n",
        "client_email": "firebase-adminsdk-prdig@cloud-firestore-test-2320f.iam.gserviceaccount.com",
        "client_id": "109287739800408928794",
        "auth_uri": "https://accounts.google.com/o/oauth2/auth",
        "token_uri": "https://oauth2.googleapis.com/token",
        "auth_provider_x509_cert_url": "https://www.googleapis.com/oauth2/v1/certs",
        "client_x509_cert_url": "https://www.googleapis.com/robot/v1/metadata/x509/firebase-adminsdk-prdig%40cloud-firestore-test-2320f.iam.gserviceaccount.com"
    }),
    databaseURL: "https://cloud-firestore-test-2320f.firebaseio.com"
});
var db = admin.firestore();

var docRef = db.collection('property-master-base');
var batch = db.batch();

global.__basedir = __dirname;
// require('./_core/upload.multipartfile.js')(app);
const storage = multer.diskStorage({
    destination: (req, file, next) => {
        next(null, __basedir + '/public/images/')
    },
    filename: (req, file, next) => {
        console.log(file); const ext = file.mimetype.split('/')[1];
        next(null, file.fieldname + "-" + file.originalname + "-" + Date.now() + '.' + ext)
    },
    //A means of ensuring only images are uploaded. 
    fileFilter: function (req, file, next) {
        if (!file) {
            next();
        }
        const image = file.mimetype.startsWith('image/');
        if (image) {
            console.log('photo uploaded');
            next(null, true);
        } else {
            console.log("file not supported");
            //TODO:  A better message response to user on failure.
            return next();
        }
    }
});
const upload = multer({ storage });

const authToken = require('./node_modules/ajlearnjs/lib/_guardHelper/authToken'); // stuck here exporting the two functions authToken & f_genIssueToken.

var nodemailer = require('nodemailer');
var transporter = nodemailer.createTransport({
    service: 'gmail',                       // 'yahoo', 'ajtestacct@yahoo.com', 'apptreme1234'
    auth: {
        user: 'ajphonehome@gmail.com',
        pass: 'NaiShanajtest612345'
    }
});
const SERVERNPORT = 'http://localhost:6707'; /* 'ajafsnode.serveo.net'; */ // from: 'ajtestacct@yahoo.com',
var vURL = ''; // to: subscriber/signup email,
var mailOptions = { // subject: 'Sending Email using Node.js',
    from: 'ajphonehome@gmail.com', // html: '<h1>Welcome</h1><p>That was easy!</p>'
    to: 'ajmindsoffire@gmail.com',
    subject: 'Please confirm account',
    html: `Please click on the following link to confirm your account:<p>${vURL}</p>` /* text: `Please confirm your account by clicking the following link: ${vURL}` */
}
transporter.sendMail(mailOptions, (error, info) => console.log({ sendMailError: error, testEmailSent: info.response }));     // test emailer.

var oldHshPW = '';
var doneOnce = false;
// const searchAllItems = JSON.parse(fs.readFileSync(`./propylist-master.json`, 'utf8'));
var searchAllItems;
const clntKYC = JSON.parse(fs.readFileSync('./b776e3e931728e48078d3e4d92ae74b5697db7af809c7c70f8524404ea99b59d.json', 'utf8'));
const tempReg = JSON.parse(fs.readFileSync('./b776e3e931728e48078d3e4d92ae74b5697db7af809c7c70f8524404ea99b59d_tempReg.json', 'utf8'));

if (!doneOnce) {
    doneOnce = true; totalCurrentListCount = 0; searchAllItems = [];

    for (let [index, agent] of clntKYC.entries()) {
        if (agent.encEmailID && agent.hshPW && agent.isDeleted === null && index !== 0) {
            let agentDistributedCMSJSONdB_FK = sha256(aj.f_rotjaG(aj.f_rotajG(agent.encEmailID) + '==' + sha256(agent.hshPW)));
            let agentDistributedCMSJSONdB = JSON.parse(fs.readFileSync(`./${agentDistributedCMSJSONdB_FK}_agentFile.json`, 'utf8'));
            console.log({ index, agentDistributedCMSJSONdB_FK, agtCode: agentDistributedCMSJSONdB[0].agent.clntAgentCode, totalCurrentListCount })
            for (let [indx, propAndAgent] of agentDistributedCMSJSONdB.entries()) {
                if (propAndAgent.is_deleted !== true && indx !== 0 && propAndAgent.id !== 0) {
                    let fillPropertyMasterList = {};
                    fillPropertyMasterList.id = propAndAgent.id;
                    fillPropertyMasterList.offerType = propAndAgent.offerType;
                    fillPropertyMasterList.proptyType = propAndAgent.proptyType;
                    fillPropertyMasterList.district = +propAndAgent.district;
                    fillPropertyMasterList.numBedRms = +propAndAgent.numBedRms;
                    fillPropertyMasterList.price = +propAndAgent.price;
                    fillPropertyMasterList.landSize = +propAndAgent.landSize;
                    fillPropertyMasterList.BUA = +propAndAgent.BUA;
                    fillPropertyMasterList.stName = propAndAgent.stName;
                    fillPropertyMasterList.listPhotoIcon = propAndAgent.listPhotoIcon;
                    fillPropertyMasterList.is_deleted = propAndAgent.is_deleted;
                    fillPropertyMasterList.legalNameID = agentDistributedCMSJSONdB[0].agent.legalNameID;
                    fillPropertyMasterList.mobileNumID = agentDistributedCMSJSONdB[0].agent.mobileNumID;
                    fillPropertyMasterList.clntAgentCode = agentDistributedCMSJSONdB[0].agent.clntAgentCode;
                    searchAllItems.push(fillPropertyMasterList);
                    totalCurrentListCount++;
                }
            }
        }
    }

    searchAllItems.map(prop => batch.set(docRef.doc(prop.clntAgentCode + prop.id), prop));
    batch.commit().then(() => console.log('Written to firestore')).catch(err => console.log('Fail', err));

    // console.log({ docRef });
    // for (let [idx, prop] of searchAllItems.entries()) {

    //     docRef.add(prop);
    // }
    // console.log('docRef', JSON.stringify({ docRef }));

    // var setAda = searchAllItems.forEach(element => {
    //     docRef.add({
    //         ...element
    //     });
    // });
    // console.log({ setAda });
    // console.log({ searchAllItems, totalCurrentListCount });
    fs.writeFile(`./propylist-master.json`, JSON.stringify(searchAllItems), 'utf8', (err) => {
        if (err) {
            console.log("::eror:: writing to propertyList-master jsonDB file."); // console.log(err); console.log(data);
        }
        console.log("::good:: updated propertyList-master jsonDB file.");
        console.log({ doneOnce, status: '..sync-ed all distributed files into master list.' });
    });
}

aj.serve.app.post('/api/plugin', (req, res) => {



    console.log('/api/plugin:');
    console.log({ 'req.body.encEmailID': req.body.encEmailID, 'req.body.hshPW': req.body.hshPW, 'req.body.rstPW': req.body.rstPW });

    let nexToken, authSIMToken, found = false;

    for (let [index, c] of clntKYC.entries()) {

        if (req.body.encEmailID === c.encEmailID) {
            found = true; console.log({ found, indexKYC: index });

            if (req.body.hshPW === c.hshPW) {
                try {
                    nexToken = aj.f_genIssueToken(req.body.encEmailID, req.body.hshPW, 'rw_');
                    authSIMToken = aj.f_genIssueToken(req.body.encEmailID, req.body.hshPW, 'rw_', 260000);
                } catch (error) {
                    res.status(400).json({ status: '..oops something broke, please try again.' });
                }
                res.status(200).json({
                    nexToken, authSIMToken, clntKYC: c,
                    status: '..welcome, you are now logged in.'
                });
            } else if (c.hshPW && !req.body.rstPW) {
                res.status(200).json({ status: '..please log in with the right password.' });
            } else if (c.hshPW && req.body.rstPW) {
                req.body.hshPW = req.body.rstPW
                res.status(200).json({ status: '..please verify your email to effect new password.' });


                let chars = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
                let token = '';
                for (let i = 23; i > 0; --i)
                    token += chars[Math.round(Math.random() * (chars.length - 1))];
                let myExpire = Date.now() / 1000 + 6 * 3600; // create expiration date /* var expires = new Date();  expires.setHours(expires.getHours() + 6); */


                for (let [idx, uc] of tempReg.entries()) {
                    if (req.body.encEmailID === uc.encEmailID && uc.emailVerified == true && !uc.isDeleted) {
                        uc.isDeleted = true;
                        oldHshPW = uc.hshPW;

                        let rstClntUser = {};
                        rstClntUser.encEmailID = req.body.encEmailID;
                        rstClntUser.hshPW = req.body.rstPW ? req.body.rstPW : null;
                        rstClntUser.emailVerified = false;
                        rstClntUser.linkToken = {
                            vEmailLinkToken: token,
                            expires: myExpire
                        };
                        rstClntUser.emailConfirmSends = 1;
                        rstClntUser.emailConfirmTries = 0;
                        vURL = SERVERNPORT + `/api/verilink/?iD=${rstClntUser.encEmailID}&vE=${rstClntUser.linkToken.vEmailLinkToken}&eX=${rstClntUser.linkToken.expires}&rsT=true`;
                        tempReg.push(rstClntUser);

                    } else if (req.body.encEmailID === uc.encEmailID && uc.emailVerified === false && !uc.isDeleted) {
                        uc.emailConfirmSends++;
                    }
                }

                mailOptions.to = aj.f_rotjaF(req.body.encEmailID);

                mailOptions.html = `<h2>Please click on the following link to confirm your <strong>Registration</strong>:</h2><p>${vURL}</p>
                <table width="100%" border="0" cellpadding="0" cellspacing="0" align="center" class="full" object="drag-module" bgcolor="#303030" c-style="not3BG">
                <tbody><tr mc:repeatable="">
                    <td align="center" style="background-image: url('${SERVERNPORT}/images/Rachela_Asciified.png'); -webkit-background-size: cover; -moz-background-size: cover; -o-background-size: cover; background-size: cover; background-position: center center; background-repeat: no-repeat; background-color: #303030;" c-style="not3BG" id="not3"></td>
                </tr></tbody></table>`;

                console.log({ mailOptionsTo: mailOptions.to, vURL });
                transporter.sendMail(mailOptions, (error, info) => {
                    if (error) { console.log(error); }
                    else {
                        console.log('Email sent: ' + info.response);
                        fs.writeFile("./b776e3e931728e48078d3e4d92ae74b5697db7af809c7c70f8524404ea99b59d_tempReg.json", JSON.stringify(tempReg), 'utf8', (err) => {
                            if (err) {
                                console.log("::eror:: writing to temp newClntUser register jsonDB.");
                                return res.status(500).json({
                                    status: '..it appears we have newClntUser service write fault, please try again later.'
                                });
                            }
                            console.log("::good:: wrote to temp newClntUser register jsonDB.");
                        });
                    }
                });
            }
        }
    }
    if (!found) {

        let foundInTemp = false;

        let chars = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        let token = '';
        for (let i = 23; i > 0; --i)
            token += chars[Math.round(Math.random() * (chars.length - 1))];
        let myExpire = Date.now() / 1000 + 6 * 3600; // create expiration date /* var expires = new Date();  expires.setHours(expires.getHours() + 6); */

        for (let [idx, uc] of tempReg.entries()) {
            if (req.body.encEmailID === uc.encEmailID) {
                foundInTemp = true;
                uc.hshPW = req.body.hshPW ? req.body.hshPW : null;
                uc.linkToken = {
                    vEmailLinkToken: token,
                    expires: myExpire
                };
                uc.emailConfirmSends++;
                vURL = SERVERNPORT + `/api/verilink/?iD=${uc.encEmailID}&vE=${uc.linkToken.vEmailLinkToken}&eX=${uc.linkToken.expires}`;
            }
        };
        console.log({ foundInTemp });

        if (!foundInTemp) {
            let newClntUser = {};
            /* console.log(tempReg); */
            newClntUser.encEmailID = req.body.encEmailID;
            newClntUser.hshPW = req.body.hshPW ? req.body.hshPW : null;
            newClntUser.emailVerified = false;
            newClntUser.linkToken = {
                vEmailLinkToken: token,
                expires: myExpire
            };
            newClntUser.emailConfirmSends = 1;
            newClntUser.emailConfirmTries = 0;
            vURL = SERVERNPORT + `/api/verilink/?iD=${newClntUser.encEmailID}&vE=${newClntUser.linkToken.vEmailLinkToken}&eX=${newClntUser.linkToken.expires}`;
            tempReg.push(newClntUser);
        }

        mailOptions.to = aj.f_rotjaF(req.body.encEmailID);

        mailOptions.html = `<h2>Please click on the following link to confirm your <strong>Registration</strong>:</h2><p>${vURL}</p>
            <table width="100%" border="0" cellpadding="0" cellspacing="0" align="center" class="full" object="drag-module" bgcolor="#303030" c-style="not3BG">
            <tbody><tr mc:repeatable="">
                <td align="center" style="background-image: url('${SERVERNPORT}/images/Rachela_Asciified.png'); -webkit-background-size: cover; -moz-background-size: cover; -o-background-size: cover; background-size: cover; background-position: center center; background-repeat: no-repeat; background-color: #303030;" c-style="not3BG" id="not3"></td>
            </tr></tbody></table>`;

        console.log({ mailOptionsTo: mailOptions.to, vURL });
        transporter.sendMail(mailOptions, (error, info) => {
            if (error) { console.log(error); }
            else {
                console.log('Email sent: ' + info.response);
                fs.writeFile("./b776e3e931728e48078d3e4d92ae74b5697db7af809c7c70f8524404ea99b59d_tempReg.json", JSON.stringify(tempReg), 'utf8', (err) => {
                    if (err) {
                        console.log("::eror:: writing to temp newClntUser register jsonDB.");
                        return res.status(500).json({
                            status: '..it appears we have newClntUser service write fault, please try again later.'
                        });
                    }
                    console.log("::good:: wrote to temp newClntUser register jsonDB.");
                    res.status(200).json({
                        status: '..thank you for signing up - please check your email to verify within 6 hours.'
                    });
                });
            }
        });
    }
});

aj.serve.app.post('/api/register-update', (req, res) => {
    console.log('/api/register-update:');
    console.log(JSON.stringify(req.body));
    let found = false;

    for (let [index, c] of clntKYC.entries()) {
        if (req.body.encEmailID == c.encEmailID && req.body.hshPW == c.hshPW) {
            found = true; console.log({ found, indexKYC: index });
            // c = req.body;    cannot force req.body to replace c log-stock-barrel
            c.encLegalNameID = req.body.encLegalNameID;
            c.gender = req.body.gender;
            c.encDOB = req.body.encDOB;
            c.encMobileNumID = req.body.encMobileNumID;
            console.log({ cGender: c.gender, reqBodyEncDOB: req.body.encDOB, c })
            if (found) {
                fs.writeFile('./b776e3e931728e48078d3e4d92ae74b5697db7af809c7c70f8524404ea99b59d.json', JSON.stringify(clntKYC), 'utf8', (err) => {
                    if (err) {
                        console.log("::eror:: writing to new clntKYC masterIndexHeadTable jsonDB."); // console.log(err); console.log(data);
                        return res.status(500).json({
                            status: '..it appears we have veriClntUser service write fault, please try again later.'
                        });
                    }
                    let updateClntUser_astFK = sha256(aj.f_rotjaG(aj.f_rotajG(c.encEmailID) + '==' + sha256(c.hshPW)));
                    let agentUpdateClntUser = JSON.parse(fs.readFileSync(`./${updateClntUser_astFK}_agentFile.json`, 'utf8'));
                    if (req.body.encLegalNameID) {
                        let names = req.body.encLegalNameID.split(/[@ ,]+/);
                        for (let i = 1; i < names.length; i++) {
                            names[0] = names[0] + ' ' + aj.f_rotjaF(names[i]);
                        }
                        agentUpdateClntUser[0].agent.legalNameID = names[0];
                    }
                    agentUpdateClntUser[0].agent.mobileNumID = aj.f_rotjaF(req.body.encMobileNumID);

                    for (let [idx, us] of searchAllItems.entries()) {
                        if (us.clntAgentCode == agentUpdateClntUser[0].agent.clntAgentCode) {
                            us.legalNameID = agentUpdateClntUser[0].agent.legalNameID;
                            us.mobileNumID = agentUpdateClntUser[0].agent.mobileNumID;
                            fs.writeFile(`./propylist-master.json`, JSON.stringify(searchAllItems), 'utf8', (err) => {
                                if (err) {
                                    console.log("::eror:: writing to propertyList-master jsonDB file."); // console.log(err); console.log(data);
                                }
                                console.log("::good:: updated propertyList-master jsonDB file.");
                            });
                        }
                    }


                    fs.writeFile(`./${updateClntUser_astFK}_agentFile.json`, JSON.stringify(agentUpdateClntUser), 'utf8', (err) => {
                        if (err) {
                            console.log("::eror:: writing to new distributed agent-cms jsonDB file."); // console.log(err); console.log(data);
                            return res.status(500).json({
                                status: '..it appears we have a distributed-cms service write fault, please try again later.'
                            });
                        }
                        console.log("::good:: wrote to new distributed agent-cms jsonDB file.");
                    });

                    console.log("::good:: wrote to new clntKYC masterIndexHeadTable jsonDB.");
                    res.status(202).json({
                        status: '..your profile records have been updated.',
                        clntKYC: c
                    });
                });

            }
        }

    }
});


aj.serve.app.get('/api/verilink', (req, res) => {
    // let tempReg = JSON.parse(fs.readFileSync('./b776e3e931728e48078d3e4d92ae74b5697db7af809c7c70f8524404ea99b59d_tempReg.json', 'utf8'));
    let newClntUser = {};
    let tocFound = false;
    let chkNowForExpires = Date.now() / 1000;
    console.log('/api/verilink:');
    console.log({ iD: req.query.iD, vE: req.query.vE, eX: req.query.eX, rsT: req.query.rsT });
    for (let [index, toc] of tempReg.entries()) {
        if (req.query.iD == toc.encEmailID) {
            console.log({ toc });
            if (req.query.vE == toc.linkToken.vEmailLinkToken && req.query.eX == toc.linkToken.expires && !toc.emailVerified && toc.isDeleted != true) {
                console.log({
                    chkNowForExpires: chkNowForExpires,
                    linkTokenExpires: toc.linkToken.expires
                });
                if (chkNowForExpires < toc.linkToken.expires) {
                    // let clntKYC = JSON.parse(fs.readFileSync('./b776e3e931728e48078d3e4d92ae74b5697db7af809c7c70f8524404ea99b59d.json', 'utf8'));
                    let veriClntUser = {
                        "encEmailID": toc.encEmailID,
                        "hshPW": toc.hshPW,
                        "subDate": null,
                        "clntCreatDate": null,
                        "clntValKYCDate": null,
                        "encLegalNameID": null,
                        "encLegalIDCred": null,
                        "photo": {
                            "photoIDFrontURL": null,
                            "photoIDBackURL": null,
                            "photoIDsURLsDated": null,
                            "recentFaceVerifiedIDURL": null,
                            "recentFaceVerifiedIDURLDated": null
                        },
                        "gender": null,
                        "encDOB": null,
                        "nationality": null,
                        "address": {
                            "postCode": null,
                            "encPostStreet": null,
                            "postBlock": null,
                            "encPostUnit": null
                        },
                        "encMobileNumID": null,
                        "bank": {
                            "encBankName": null,
                            "bankScanStatemtURL": null,
                            "bankScanStatemtURLDated": null,
                            "encBankScanStatemtBal": null,
                            "encBankAcct": null
                        },
                        "textAnnotNotaryOthers": null,
                        "clntNotes": null,
                        "isDeleted": null,
                        "clntAgentCode": `${aj.f_rotjaF(toc.encEmailID)}@JScorp`
                    };
                    console.log({ 'req.query.rsT': req.query.rsT });
                    console.log(req.query.rsT != 'true');
                    if (req.query.rsT != 'true') {
                        toc.hshPW == null ? veriClntUser.subDate = new Date() : veriClntUser.clntCreatDate = new Date();
                        clntKYC.push(veriClntUser);
                    } else if (req.query.rsT == 'true') {
                        for (let [index, c] of clntKYC.entries()) {
                            let found = false;
                            if (req.query.iD == c.encEmailID) {
                                found = true;
                                console.log({ found, indexKYC: index });
                                c.hshPW = toc.hshPW
                            }
                        }
                    }

                    fs.writeFile('./b776e3e931728e48078d3e4d92ae74b5697db7af809c7c70f8524404ea99b59d.json', JSON.stringify(clntKYC), 'utf8', (err) => {
                        if (err) {
                            console.log("::eror:: writing to new clntKYC masterIndexHeadTable jsonDB."); // console.log(err); console.log(data);
                            return res.status(500).json({
                                status: '..it appears we have veriClntUser service write fault, please try again later.'
                            });
                        }
                        console.log("::good:: wrote to new clntKYC masterIndexHeadTable jsonDB.");
                        toc.emailVerified = true;
                        toc.emailConfirmTries++;
                        fs.writeFile('./b776e3e931728e48078d3e4d92ae74b5697db7af809c7c70f8524404ea99b59d_tempReg.json', JSON.stringify(tempReg), 'utf8', (err) => {
                            if (err) {
                                console.log("::eror:: writing to temp newClntUser register jsonDB."); // console.log(err); console.log(data);
                                return res.status(500).json({
                                    status: '..it appears we have veriClntUser service write fault, please try again later.'
                                });
                            }
                            console.log("::good:: wrote to temp newClntUser register jsonDB.");
                        });


                        if (req.query.rsT != 'true') {
                            let agentClntUser = [{
                                agent: {
                                    "encEmailID": toc.encEmailID,
                                    "hshPW": toc.hshPW,
                                    "clntCreatDate": veriClntUser.clntCreatDate,
                                    "clntAgentCode": `${aj.f_rotjaF(toc.encEmailID)}@JScorp`,
                                    "legalNameID": null,
                                    "mobileNumID": null
                                }
                            }, {
                                "id": 0,
                                "offerType": "",
                                "proptyType": "",
                                "district": "",
                                "numBedRms": "",
                                "price": "",
                                "landSize": "",
                                "BUA": "",
                                "stName": "",
                                "listPhotoIcon": "",
                                "ownerName": "ADD NEW",
                                "ownerMobile": "",
                                "ownerEmail": "",
                                "propyFullAddr": "",
                                "postCode": "",
                                "is_deleted": false
                            }];
                            veriClntUser.astFK = sha256(aj.f_rotjaG(aj.f_rotajG(toc.encEmailID) + '==' + sha256(toc.hshPW)));
                            fs.writeFile(`./${veriClntUser.astFK}_agentFile.json`, JSON.stringify(agentClntUser), 'utf8', (err) => {
                                if (err) {
                                    console.log("::eror:: writing to new distributed agent-cms jsonDB file."); // console.log(err); console.log(data);
                                    return res.status(500).json({
                                        status: '..it appears we have a distributed-cms service write fault, please try again later.'
                                    });
                                }
                                console.log("::good:: wrote to new distributed agent-cms jsonDB file.");
                            });

                        } else if (req.query.rsT == 'true') {       // copy contents of old file into new with reset of hshPW
                            console.log({ oldHshPW });
                            veriClntUser.astFK = sha256(aj.f_rotjaG(aj.f_rotajG(toc.encEmailID) + '==' + sha256(oldHshPW)));
                            let agentRstClntUser = JSON.parse(fs.readFileSync(`./${veriClntUser.astFK}_agentFile.json`, 'utf8'));
                            agentRstClntUser[0].agent.hshPW = toc.hshPW;               // change hshPW in the new agent file.
                            veriClntUser.astFK = sha256(aj.f_rotjaG(aj.f_rotajG(toc.encEmailID) + '==' + sha256(toc.hshPW)));
                            fs.writeFile(`./${veriClntUser.astFK}_agentFile.json`, JSON.stringify(agentRstClntUser), 'utf8', (err) => {
                                if (err) {
                                    console.log("::eror:: writing to new, replaced distributed agent-cms jsonDB file."); // console.log(err); console.log(data);
                                    return res.status(500).json({
                                        status: '..it appears we have a distributed-cms service write fault, please try again later.'
                                    });
                                }
                                console.log("::good:: wrote to new, replaced distributed agent-cms jsonDB file.");
                            });
                        }

                        tocFound = true;
                        res.status(200).json({
                            status: 'Hi, I\'m Jane:..thank you for signing up / resetting your password - you can now log in.'
                        });
                        // res.status(200).sendFile(__dirname + '/public' + '/images/ascii' + '.html');
                    });



                }
            } else if (req.query.eX < chkNowForExpires && !toc.emailVerified && toc.isDeleted != true) {
                let chars = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ'; // let chars = JSON.parse(fs.readFileSync('./specGCodes.json', 'utf8'));
                let token = '';
                for (let i = 23; i > 0; --i) {
                    token += chars[Math.round(Math.random() * (chars.length - 1))];
                }
                let myExpire = Date.now() / 1000 + 6 * 3600;

                toc.linkToken = {
                    vEmailLinkToken: token,
                    expires: myExpire
                };
                vURL = SERVERNPORT + `/api/verilink/?iD=${toc.encEmailID}&vE=${toc.linkToken.vEmailLinkToken}&eX=${toc.linkToken.expires}`;

                mailOptions.to = aj.f_rotjaF(toc.encEmailID);
                let registerType = req.body.hshPW ? 'signup' : 'free subscription';
                /* mailOptions.text = `Please confirm your account by clicking the following link: ${vURL}`; */
                mailOptions.html = `<h2>Please click on the following link to confirm your <strong>${registerType}</strong>:</h2><p>${vURL}</p>
                <table width="100%" border="0" cellpadding="0" cellspacing="0" align="center" class="full" object="drag-module" bgcolor="#303030" c-style="not3BG">
                <tbody><tr mc:repeatable="">
                    <td align="center" style="background-image: url('${SERVERNPORT}/images/Rachela_Asciified.png'); -webkit-background-size: cover; -moz-background-size: cover; -o-background-size: cover; background-size: cover; background-position: center center; background-repeat: no-repeat; background-color: #303030;" c-style="not3BG" id="not3">
                        <div mc:hideable="">
    
                                
                        </div>
                    </td>
                </tr>
                </tbody></table>`;

                console.log({ mailOptionsTo: mailOptions.to, vURL, status: '..sent fresh email verification link since last try has already expired after 6 hours.' });// status not sent to client browser, for server info only.
                transporter.sendMail(mailOptions, (error, info) => {
                    if (error) { console.log(error); }
                    else {
                        console.log('Email sent: ' + info.response);
                        toc.emailConfirmSends++;
                        toc.emailConfirmTries++;
                        fs.writeFile("./b776e3e931728e48078d3e4d92ae74b5697db7af809c7c70f8524404ea99b59d_tempReg.json", JSON.stringify(tempReg), 'utf8', (err) => {
                            if (err) {
                                console.log("::eror:: writing to temp newClntUser register jsonDB."); // console.log(err); console.log(data);
                                return res.status(500).json({
                                    status: '..it appears we have newClntUser service write fault, please try again later.'
                                });
                            }
                            console.log("::good:: wrote to temp newClntUser register jsonDB.");
                        });
                    }
                });
            } else if (req.query.eX > chkNowForExpires && !toc.emailVerified && toc.isDeleted != true) {
                toc.emailConfirmTries++;
                fs.writeFile("./b776e3e931728e48078d3e4d92ae74b5697db7af809c7c70f8524404ea99b59d_tempReg.json", JSON.stringify(tempReg), 'utf8', (err) => {
                    if (err) {
                        console.log("::eror:: writing to temp newClntUser register jsonDB."); // console.log(err); console.log(data);
                        return res.status(500).json({
                            status: '..it appears we have newClntUser service write fault, please try again later.'
                        });
                    }
                    console.log("::good:: wrote to temp newClntUser register jsonDB.");
                });
                return res.status(202).json({
                    status: '..please verify on the latest email sent out, or click on \'signup\' again to get a latest email verification request.'
                });

            } else if (toc.emailVerified && toc.isDeleted != true) {
                return res.status(201).json({
                    status: '..your email have already been verified.'
                });
            }

        }
    }
});

aj.serve.app.get('/api/search-list', (req, res) => {
    let listCount = 0; let list = 'propylist-master'; let clntAssetList = [];
    console.log('/api/search-list:[GET]');
    console.log('req.query:', JSON.stringify(req.query));
    console.log('req.query.crud:', req.query.crud);
    console.log('req.query.searchPrice:', req.query.searchPrice);

    if (req.query.key1 != 'null' && req.query.key2 != 'null' && req.query.crud != 'getAll') {
        clntAssetList = []; listCount = 0;
        let clntKYC = JSON.parse(fs.readFileSync(`./b776e3e931728e48078d3e4d92ae74b5697db7af809c7c70f8524404ea99b59d.json`, 'utf8'));
        let listClntUser_astFK = sha256(aj.f_rotjaG(aj.f_rotajG(req.query.key1) + '==' + sha256(req.query.key2)));
        let agentListClntUser = JSON.parse(fs.readFileSync(`./${listClntUser_astFK}_agentFile.json`, 'utf8'));
        let found = false; let list = false;
        for (let [index, c] of clntKYC.entries()) {
            if (req.query.key1 == c.encEmailID && req.query.key2 == c.hshPW) {
                found = true; let ownerList = []; let ownerDet = {};
                console.log({ found, indexKYC: index });

                // console.log({ listClntUser_astFK, agentListClntUser });
                for (let [idx, l] of agentListClntUser.entries()) {
                    ownerDet = {};
                    if (req.query.crud == l.ownerName && !l.is_deleted && req.query.searchPrice === undefined) {
                        list = listClntUser_astFK; listCount++;
                        console.log({ list, idxList: idx, listCount });
                        clntAssetList.push(l);

                    }
                    if (req.query.searchType == l.offerType &&
                        req.query.typeOfProptySelected == l.proptyType &&
                        req.query.districtSelected == l.district &&
                        req.query.bedrmsSelected == l.numBedRms &&
                        req.query.searchPrice >= +l.price &&
                        !l.is_deleted && req.query.crud === undefined) {
                        list = listClntUser_astFK; listCount++;
                        console.log({ list, idxList: idx, listCount });
                        agentListClntUser[idx].clntAgentCode = agentListClntUser[0].agent.clntAgentCode;
                        agentListClntUser[idx].legalNameID = agentListClntUser[0].agent.legalNameID;
                        agentListClntUser[idx].mobileNumID = agentListClntUser[0].agent.mobileNumID;
                        clntAssetList.push(agentListClntUser[idx]);
                    }
                    if (l.ownerName && !l.is_deleted && req.query.crud === undefined) {
                        ownerDet.ownerName = l.ownerName;
                        ownerDet.ownerMobile = l.ownerMobile;
                        ownerDet.ownerEmail = l.ownerEmail;
                        ownerDet.propyFullAddr = l.propyFullAddr;
                        ownerDet.postCode = l.postCode;
                        ownerList.push(ownerDet);
                    }

                }
                console.log(`${list}>> agentList found items:`, listCount);
                return res.status(200).json({
                    status: `..your agent search returned with ${listCount} items.`,
                    clntAssetList,
                    listCount,
                    ownerList
                });
            }
        }
    } else if (req.query.crud == 'getAll') {
        clntAssetList = []; listCount = 0;
        for (let [idx, l] of searchAllItems.entries()) {
            if (l.is_deleted != true) {
                listCount++;
                clntAssetList.push(l);
            }
        }
        console.log(`propylist-master>> userList found items:`, listCount);
        return res.status(200).json({
            status: `..your user browseAll returned with ${listCount} items.`,
            clntAssetList,
            listCount
        });
    } else if (req.query.key1 == 'null' && req.query.key2 == 'null') {
        clntAssetList = []; listCount = 0;
        // let searchAllItems = JSON.parse(fs.readFileSync(`./${list}.json`, 'utf8'));
        for (let [idx, l] of searchAllItems.entries()) {
            if (req.query.searchType == l.offerType &&
                req.query.typeOfProptySelected == l.proptyType &&
                req.query.districtSelected == l.district &&
                req.query.bedrmsSelected == l.numBedRms &&
                req.query.searchPrice >= +l.price &&
                !l.is_deleted) {
                listCount++;
                console.log({ list, idxList: idx, listCount });
                clntAssetList.push(l);
            }
        }
        console.log(`${list}>> userList found items:`, listCount);
        return res.status(200).json({
            status: `..your user search returned with ${listCount} items.`,
            clntAssetList,
            listCount
        });
    }
});

aj.serve.app.post('/api/search-list', (req, res) => {
    let listCount = 0; let list = 'propylist-master'; let clntAssetList = [];
    console.log('/api/search-list:');
    console.log('req.body:', JSON.stringify(req.body));
    if (req.body.encEmailID && req.body.hshPW) {
        clntAssetList = []; listCount = 0;
        let clntKYC = JSON.parse(fs.readFileSync(`./b776e3e931728e48078d3e4d92ae74b5697db7af809c7c70f8524404ea99b59d.json`, 'utf8'));
        let listClntUser_astFK = sha256(aj.f_rotjaG(aj.f_rotajG(req.body.encEmailID) + '==' + sha256(req.body.hshPW)));
        let agentListClntUser = JSON.parse(fs.readFileSync(`./${listClntUser_astFK}_agentFile.json`, 'utf8'));
        let found = false; let list = false;
        for (let [index, c] of clntKYC.entries()) {
            if (req.body.encEmailID == c.encEmailID && req.body.hshPW == c.hshPW) {
                found = true; let ownerList = []; let ownerDet = {};
                console.log({ found, indexKYC: index });

                // console.log({ listClntUser_astFK, agentListClntUser });
                for (let [idx, l] of agentListClntUser.entries()) {
                    ownerDet = {};
                    if (req.body.clntUserSearch.searchType == l.offerType &&
                        req.body.clntUserSearch.typeOfProptySelected == l.proptyType &&
                        req.body.clntUserSearch.districtSelected == l.district &&
                        req.body.clntUserSearch.bedrmsSelected == l.numBedRms &&
                        req.body.clntUserSearch.searchPrice >= +l.price &&
                        !l.is_deleted) {
                        list = listClntUser_astFK; listCount++;
                        console.log({ list, idxList: idx, listCount });
                        l.clntAgentCode = agentListClntUser[0].agent.clntAgentCode;
                        l.legalNameID = agentListClntUser[0].agent.legalNameID;
                        l.mobileNumID = agentListClntUser[0].agent.mobileNumID;
                        clntAssetList.push(l);
                    }
                    if (l.ownerName && !l.is_deleted) {
                        ownerDet.ownerName = l.ownerName;
                        ownerDet.ownerMobile = l.ownerMobile;
                        ownerDet.ownerEmail = l.ownerEmail;
                        ownerDet.propyFullAddr = l.propyFullAddr;
                        ownerDet.postCode = l.postCode;
                        ownerList.push(ownerDet);
                    }

                }
                console.log(`${list}>> agentList found items:`, listCount);
                return res.status(200).json({
                    status: `..your agent search returned with ${listCount} items.`,
                    clntAssetList,
                    listCount,
                    ownerList
                });
            }
        }
    } else {
        clntAssetList = []; listCount = 0;
        // let searchAllItems = JSON.parse(fs.readFileSync(`./${list}.json`, 'utf8'));
        for (let [idx, l] of searchAllItems.entries()) {
            if (req.body.clntUserSearch.searchType == l.offerType &&
                req.body.clntUserSearch.typeOfProptySelected == l.proptyType &&
                req.body.clntUserSearch.districtSelected == l.district &&
                req.body.clntUserSearch.bedrmsSelected == l.numBedRms &&
                req.body.clntUserSearch.searchPrice >= +l.price &&
                !l.is_deleted) {
                listCount++;
                console.log({ list, idxList: idx, listCount });
                clntAssetList.push(l);
            }
        }
        console.log(`${list}>> userList found items:`, listCount);
        return res.status(200).json({
            status: `..your user search returned with ${listCount} items.`,
            clntAssetList,
            listCount
        });
    }

});

aj.serve.app.get('/api/wealthmore-meter', authToken, (req, res) => {

    try {
        let clntUserAsset = JSON.parse(fs.readFileSync(`./${req.astFK}.json`, 'utf8'));
        res.ast = aj.f_rotajF(JSON.stringify(clntUserAsset));
        console.log(JSON.stringify(clntUserAsset));
        console.log({
            nexToken: res.nexToken,
            clntUserAsset: res.ast
        });
        res.status(200).json({
            nexToken: res.nexToken,
            clntUserAsset: res.ast
        });

    } catch (error) {
        res.status(400).json({
            status: '..please try again later..our servers may be overwhelmed.'
        });
    }
});

aj.serve.app.post('/images', upload.single("proptyPic"), (req, res) => {
    console.log('/images:[POST]');
    console.log('req.body:', JSON.stringify(req.body));
    console.log('req.file:', req.file);
    if (req.body.encEmailID && req.body.hshPW) {
        clntAssetList = []; listCount = 0;
        let clntKYC = JSON.parse(fs.readFileSync(`./b776e3e931728e48078d3e4d92ae74b5697db7af809c7c70f8524404ea99b59d.json`, 'utf8'));
        let updateClntUser_astFK = sha256(aj.f_rotjaG(aj.f_rotajG(req.body.encEmailID) + '==' + sha256(req.body.hshPW)));
        let agentUpdateClntUser = JSON.parse(fs.readFileSync(`./${updateClntUser_astFK}_agentFile.json`, 'utf8'));
        let found = false; let list = false;
        for (let [index, c] of clntKYC.entries()) {
            if (req.body.encEmailID == c.encEmailID && req.body.hshPW == c.hshPW) {
                found = true; let ownerList = []; let ownerDet = {};
                console.log({ found, indexKYC: index });

                // console.log({ listClntUser_astFK, agentListClntUser });
                for (let [idx, l] of agentUpdateClntUser.entries()) {
                    ownerDet = {};
                    if (req.body.crud == l.ownerName) {

                        if (req.body.propID == l.id && l.id != 0) {
                            l.listPhotoIcon = req.file.filename;
                            for (let [idx1, all] of searchAllItems.entries()) {
                                if (l.id == all.id && agentUpdateClntUser[0].agent.clntAgentCode == searchAllItems[idx1].clntAgentCode) {
                                    all.listPhotoIcon = l.listPhotoIcon;
                                    docRef.doc(searchAllItems[idx1].clntAgentCode + searchAllItems[idx1].id).set(searchAllItems[idx1]);
                                    fs.writeFile(`./propylist-master.json`, JSON.stringify(searchAllItems), 'utf8', (err) => {
                                        if (err) {
                                            console.log("::eror:: writing to propertyList-master jsonDB file."); // console.log(err); console.log(data);
                                        }
                                        console.log("::good:: updated propertyList-master jsonDB file.");
                                    });
                                }
                            }
                        }

                        list = updateClntUser_astFK; listCount++;
                        console.log({ list, idxList: idx, listCount });
                        clntAssetList.push(l);
                    }
                }
                fs.writeFile(`./${updateClntUser_astFK}_agentFile.json`, JSON.stringify(agentUpdateClntUser), 'utf8', (err) => {
                    if (err) {
                        console.log("::eror:: writing to new distributed agent-cms jsonDB file."); // console.log(err); console.log(data);
                        return res.status(500).json({
                            status: '..it appears we have a distributed-cms service write fault, please try again later.'
                        });
                    }
                    console.log("::good:: wrote to new distributed agent-cms jsonDB file.");
                });
                console.log(`${list}>> agentList found items:`, listCount);
                return res.status(200).json({
                    status: `..your property image was uploaded and updated successfully!`,
                    clntAssetList,
                    listCount
                });
            }
        }
    }
    res.json({ 'msg': 'File uploaded successfully!', 'file': req.file });
});

aj.serve.app.post('/api/add-save', (req, res) => {

    console.log('/api/add-save:[POST]');
    console.log('req.body:', JSON.stringify(req.body));
    var updatePropyMasterList = {};
    if (req.body.encEmailID && req.body.hshPW) {
        clntAssetList = []; listCount = 0; ownerListLength = 0;
        let clntKYC = JSON.parse(fs.readFileSync(`./b776e3e931728e48078d3e4d92ae74b5697db7af809c7c70f8524404ea99b59d.json`, 'utf8'));
        let updateClntUser_astFK = sha256(aj.f_rotjaG(aj.f_rotajG(req.body.encEmailID) + '==' + sha256(req.body.hshPW)));
        let agentUpdateClntUser = JSON.parse(fs.readFileSync(`./${updateClntUser_astFK}_agentFile.json`, 'utf8'));
        let found = false; let list = false;
        for (let [index, c] of clntKYC.entries()) {
            if (req.body.encEmailID == c.encEmailID && req.body.hshPW == c.hshPW) {
                found = true; let ownerList = []; let ownerDet = {};
                console.log({ found, indexKYC: index });

                // console.log({ listClntUser_astFK, agentListClntUser });
                for (let [idx, l] of agentUpdateClntUser.entries()) {
                    ownerListLength++;
                    if (req.body.ownerName == l.ownerName && l.id != 0) {

                        for (let [idex, ul] of req.body.myclntUpdateList.entries()) {
                            if (ul.id == l.id) {
                                ul.ownerName = req.body.myclntUpdateList[0].ownerName.toUpperCase();
                                ul.ownerMobile = req.body.myclntUpdateList[0].ownerMobile;
                                ul.ownerEmail = req.body.myclntUpdateList[0].ownerEmail;
                                agentUpdateClntUser[idx] = ul;
                                console.log({ agentUpdateClntUser, ul });
                            }
                        }
                        for (let [idx1, all] of searchAllItems.entries()) { // l and all are internal counters.
                            if (l.id == all.id && agentUpdateClntUser[0].agent.clntAgentCode == searchAllItems[idx1].clntAgentCode) {
                                searchAllItems[idx1].id = agentUpdateClntUser[idx].id;
                                searchAllItems[idx1].offerType = agentUpdateClntUser[idx].offerType;
                                searchAllItems[idx1].proptyType = agentUpdateClntUser[idx].proptyType;
                                searchAllItems[idx1].district = +agentUpdateClntUser[idx].district;
                                searchAllItems[idx1].numBedRms = +agentUpdateClntUser[idx].numBedRms;
                                searchAllItems[idx1].price = +agentUpdateClntUser[idx].price;
                                searchAllItems[idx1].landSize = +agentUpdateClntUser[idx].landSize;
                                searchAllItems[idx1].BUA = +agentUpdateClntUser[idx].BUA;
                                searchAllItems[idx1].stName = agentUpdateClntUser[idx].stName;
                                searchAllItems[idx1].listPhotoIcon = agentUpdateClntUser[idx].listPhotoIcon;
                                searchAllItems[idx1].is_deleted = agentUpdateClntUser[idx].is_deleted;
                                searchAllItems[idx1].legalNameID = agentUpdateClntUser[0].agent.legalNameID;
                                searchAllItems[idx1].mobileNumID = agentUpdateClntUser[0].agent.mobileNumID;
                                searchAllItems[idx1].clntAgentCode = agentUpdateClntUser[0].agent.clntAgentCode;

                                docRef.doc(searchAllItems[idx1].clntAgentCode + searchAllItems[idx1].id).set(searchAllItems[idx1]);
                            }
                        }
                        list = updateClntUser_astFK; listCount++;
                        console.log({ list, idxList: idx, listCount });
                        // clntAssetList.push(l);
                    }
                }

                if (req.body.ownerName == 'ADD NEW') {
                    req.body.myclntUpdateList[0].ownerName = req.body.myclntUpdateList[0].ownerName.toUpperCase();
                    req.body.myclntUpdateList[0].id = ownerListLength - 1;
                    agentUpdateClntUser.push(req.body.myclntUpdateList[0]);

                    updatePropyMasterList.id = req.body.myclntUpdateList[0].id;
                    updatePropyMasterList.offerType = req.body.myclntUpdateList[0].offerType;
                    updatePropyMasterList.proptyType = req.body.myclntUpdateList[0].proptyType;
                    updatePropyMasterList.district = +req.body.myclntUpdateList[0].district;
                    updatePropyMasterList.numBedRms = +req.body.myclntUpdateList[0].numBedRms;
                    updatePropyMasterList.price = +req.body.myclntUpdateList[0].price;
                    updatePropyMasterList.landSize = +req.body.myclntUpdateList[0].landSize;
                    updatePropyMasterList.BUA = +req.body.myclntUpdateList[0].BUA;
                    updatePropyMasterList.stName = req.body.myclntUpdateList[0].stName;
                    updatePropyMasterList.listPhotoIcon = req.body.myclntUpdateList[0].listPhotoIcon;
                    updatePropyMasterList.is_deleted = req.body.myclntUpdateList[0].is_deleted;
                    updatePropyMasterList.legalNameID = agentUpdateClntUser[0].agent.legalNameID;
                    updatePropyMasterList.mobileNumID = agentUpdateClntUser[0].agent.mobileNumID;
                    updatePropyMasterList.clntAgentCode = agentUpdateClntUser[0].agent.clntAgentCode;
                    searchAllItems.push(updatePropyMasterList);
                    docRef.doc(updatePropyMasterList.clntAgentCode + updatePropyMasterList.id).set(updatePropyMasterList);
                }

                fs.writeFile(`./${updateClntUser_astFK}_agentFile.json`, JSON.stringify(agentUpdateClntUser), 'utf8', (err) => {
                    if (err) {
                        console.log("::eror:: writing to new distributed agent-cms jsonDB file."); // console.log(err); console.log(data);
                        return res.status(500).json({
                            status: '..it appears we have a distributed-cms service write fault, please try again later.'
                        });
                    }
                    console.log("::good:: wrote to new distributed agent-cms jsonDB file.");
                    fs.writeFile(`./propylist-master.json`, JSON.stringify(searchAllItems), 'utf8', (err) => {
                        if (err) {
                            console.log("::eror:: writing to propertyList-master jsonDB file."); // console.log(err); console.log(data);
                        }
                        console.log("::good:: updated propertyList-master jsonDB file.");
                    });
                });
                console.log(`${list}>> agentList found items:`, listCount);
                return res.status(200).json({
                    status: `..your new owner&property or properties-update-saveAll was uploaded and updated successfully!`
                    // myclntUpdateList: req.body.myclntUpdateList
                });
            }
        }
    }
});

// console.log(aj.f_genIssueToken('jskJIrKKswx@LjHsP.nrj', '3cc98b78aabdbb22273c4679870ad2e05c8672707675734f0ea2fbef6618d651', 'rw_'));
console.log(aj.f_genIssueToken('jskJIrKKswx@LjHsP.nrj', null, 'rw_'));
console.log(aj.f_genIssueToken('jskJIrKKswx@LjHsP.nrj', null, 'rw_', 260000));
// console.log(aj.f_genIssueToken('jskJIrKKswx@LjHsP.nrj', '', 'rw_'));
console.log(sha256(aj.f_rotjaG(aj.f_rotajG('jskJIrKKswx2@LjHsP.nrj') + '==' + sha256('null'))));
console.log(sha256(aj.f_rotjaG(aj.f_rotajG('HkJwxyvHk@Hqqvwxjx.IL') + '==' + sha256('null'))));


// aj.serve.app.use(aj.serve.express.static(__dirname + '/dist'));

aj.serve.app.use(aj.serve.express.static(__dirname + '/public'));
// index must come after /public for images to be seen from /public/images
// aj.serve.app.get('/*', (req, res) => {
//     res.sendFile(path.join(__dirname + '/dist/index.html'));
// });
// aj.serve.app.get('/*', (req, res) => {
//     res.sendFile(path.join(__dirname + '/public/index.html'));
// });

aj.serve.app.use((req, res, next) => {
    res.redirect('/error.html');
});